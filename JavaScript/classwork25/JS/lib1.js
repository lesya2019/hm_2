document.addEventListener('DOMContentLoaded', onReady);

function onReady() {
    const button = document.querySelector('div.container > button');
    const newP = document.createElement('p');

    newP.innerHTML = 'Some text!!!';
    button.parentNode.insertBefore(newP, button);

    // --- //
    const btns = document.getElementsByTagName('button');
    for(let btn of btns) {
        btn.addEventListener('click', onBtnClick);
    }
}

// ---- //

function onBtnClick() {
    console.log(this);
}

