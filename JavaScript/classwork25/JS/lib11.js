document.addEventListener('DOMContentLoaded', onReady);

/**
 * @desc Реакция на построение DOM
 **/
function onReady() {
    const inputs = document.getElementsByTagName('input');

    for(let input of inputs) {
        input.addEventListener('keydown', onInputKeyPress);
    }
}

/**
 * @desc обработка нажатия клавиши
 **/
function onInputKeyPress(e) {
    const char = String.fromCharCode(e.keyCode);

    this.nextElementSibling.innerHTML = char;
}
