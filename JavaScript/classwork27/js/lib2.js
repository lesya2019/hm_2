document.addEventListener('DOMContentLoaded', onReady);
let timer=null;
function showMsg (msgText, time) {
    setTimeout(function () {
        alert(msgText);
    }, time);
}
function onReady() {
    const span= document.getElementById('timer');
    span.innerHTML=0;
    timer=setInterval(function () {
        span.innerHTML=+span.innerHTML+1;
    }, 1000);

    showMsg('Hello', 3000)
}
function onButtonClick() {
    if (timer) {
        clearInterval(timer);
    }
}