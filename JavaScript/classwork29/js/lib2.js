let div = document.querySelector('.container');

//div.addEventListener('mouseover', onMouseOver);

function onMouseOver(event) {
    let element = event.target;
    if (element === event.currentTarget) {
        event.preventDefault();
        return;
    }
    element.style.backgroundColor = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`;
}

function buildColor() {
    return `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`
}

$('.container > div')
    .mouseover(function (e) {
        $(this).css('background-color', buildColor())
    })
    .click(function () {
        $(this)
            .css('border-color', buildColor())
            .css('border-size', Math.random() * 10);

    })
    .find('input')
    .focus(function () {
        $(this).css('color', buildColor())
    })
