let div = document.querySelector('.container');

div.addEventListener('mouseover', onMouseOver);

function onMouseOver(event) {
    let element = event.target;
    if (element === event.currentTarget) {
        event.preventDefault();
        return;
    }
    element.style.backgroundColor = `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`;
}
div.addEventListener('click', onClick);
function onClick(e) {
    let element = e.target;
    if (element === e.currentTarget) {
        e.preventDefault();
        return;
    }
    element.style.borderColor = buildColor();
    element.style.borderWidth = `${Math.random()*20}px`;
}
div.querySelector('input').addEventListener('focus',onFocus);

function onFocus(e) {
    const element = e.target;
    element.style.color = buildColor();

}

function buildColor() {
    return `rgb(${Math.random() * 255}, ${Math.random() * 255}, ${Math.random() * 255})`
}

// $('.container > div')
//     .mouseover(function (e) {
//         $(this).css('background-color', buildColor())
//     })
//     .click(function () {
//         $(this)
//             .css('border-color', buildColor())
//             .css('border-size', Math.random() * 10);
//
//     })
//     .find('input')
//     .focus(function () {
//         $(this).css('color', buildColor())
//     })
